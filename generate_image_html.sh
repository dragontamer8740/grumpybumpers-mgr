#! /usr/bin/env sh
gen() {
  cat << EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title>Signature Images</title>
  <style>
    body {
      background-color: #C0C0C0;
      text-align: center;
      justify-content: space-around;
    }
    p {
      text-align: left;
    }
    img {
      margin: 4px;
      border: 2px solid blue;
    }
    .section {
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
    }
    .imgwrap {
      display: inline-block;
      overflow-x: auto;
      overflow-y: hidden;
    }
    .linkbox {
      display: block;
      text-align: center;
      margin: 0 auto;
    }
    .imgwrap img {
      display: inline-block;
      text-align: center;
    }
    .imgwrap, .captionwrap {
      text-align: center;
      margin: 0 auto;
    }
    .imgcaption {
      display: block;
      max-width: 98%;
      text-align: center;
    }
    h2 {
      text-align: left;
    }
  </style>
  </head>
  <body>
EOF
  OLDDESC=''
  COUNTER=''
  while read -r line; do
    echo "$line" | grep -q '\s'
    if [ "$?" -eq 0 ]; then
      URL="$(echo "$line" | awk '{ print $1 }')"
      DESC="$(echo "$line" | awk '{ $1=""; print $0 }'|sed 's/^\s//')"
      if [ "$DESC" ]; then
        # if DESC is non-empty string, update OLDDESC (if needed)
        if [ "$OLDDESC" != "$DESC" ]; then
          if [ "$COUNTER" ]; then
            echo '</div>'
            echo '<hr/>'
            echo '<h2>'"$DESC"'</h2>'
            echo '<div class="section">'
          else
            COUNTER='1'
            echo '<h2>'"$DESC"'</h2>'
            echo '<div class="section">'
          fi
          OLDDESC="$DESC"
        fi
      fi
    else
      URL="$line"
      DESC=''
    fi
    if [ "$DESC" ]; then
      #echo '    <div class="imgwrap"><span class="linkbox"><a href="'"$URL"'"><img src="'"$URL"'" alt="sigpic"/></a></span><div class="imgcaption">'"$DESC"'</div></div>'
      # echo '    <a href="'"$URL"'"><img src="'"$URL"'" alt="sigpic"/></a>'
      echo '    <div class="imgwrap"><span class="linkbox"><a href="'"$URL"'"><img src="'"$URL"'" alt="sigpic"/></a></span></div>'
    else
      echo '    <div class="imgwrap"><span class="linkbox"><a href="'"$URL"'"><img src="'"$URL"'" alt="sigpic"/></a></span></div>'
    fi
  done
  cat << EOF
</div>
<hr/>
<p>
  <i>Full source code for my automatic HTML generator (for this page) and
  Grumpybumpers signature management tool can both be found
  <a href="https://gitlab.com/dragontamer8740/grumpybumpers-mgr">on Gitlab</a>.</i>
</p>
<p>
  <i>(Clone URL:
  <a href="https://gitlab.com/dragontamer8740/grumpybumpers-mgr.git">https://gitlab.com/dragontamer8740/grumpybumpers-mgr.git</a>)</i>
</p>
</body>
</html>
EOF
}
if [ "$#" -gt 0 ]; then
  ./unique-only < "$1" | gen
  # sort -V < "$1" | uniq | gen
else
  ./unique-only | gen
  # sort -V | uniq | gen
fi
