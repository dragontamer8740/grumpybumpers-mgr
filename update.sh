#! /usr/bin/env sh
AUTHCONF='./auth.cfg'
SETTINGSCONF='./settings.cfg'

PRETTYPROG='python3 ./pretty.py'
# settings.cfg can override things (like PRETTYPROG or AUTHCONF):
if [ -e "SETTINGSCONF" ]; then
  . "$SETTINGSCONF"
fi

if [ -e "$AUTHCONF" ]; then
  . "$AUTHCONF"
fi

if [ "$username" ] && [ "$password" ]; then
  DEBUG=''
  SIGNO=1
  IMAGESCONF='images.cfg'
  if [ "$#" -gt 0 ]; then
    IMAGESCONF="$1"
  fi
  
  FORMDATA=''

  while read -r line; do
    if [ "$line" ]; then # if line not empty
      echo "$line" | grep -q '\s'
      if [ "$?" -eq 0 ]; then
        # When a line contains a whitespace character, whatever follows the
        # whitespace is a description
        DESC="$(echo "$line" | awk '{ $1=""; print $0 }'|sed 's/^\s//')"
        URL="$(echo "$line" | awk '{ print $1 }')"
      else
        URL="$line"
      fi
      FORMDATA="$FORMDATA"' -F sig_'"$SIGNO"'='"$URL"
      SIGNO=`expr "$SIGNO" '+' '1'`
    fi
  done < "$IMAGESCONF"
  FORMDATA="$FORMDATA"' -F numsigs='`expr "$SIGNO" '+' '9'`
  echo "$FORMDATA"
  curl $DEBUG \
       --silent \
       -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0 SeaMonkey/2.53.7' \
       -H 'Referer: https://sig.grumpybumpers.com/' \
       -F 'username='"$username" \
       -F 'password='"$password" \
       $FORMDATA \
       -F '.submit="Submit Query"' \
       'https://sig.grumpybumpers.com/' | \
    $PRETTYPROG
  ./generate_image_html.sh < "$IMAGESCONF" > sigpic-site/index.html
  cd sigpic-site
  git add index.html
  git commit -m 'bumped index.html'
  git push
else
  1>&2 echo "Error: User name and password not provided in auth.cfg, or auth.cfg not found."
fi
