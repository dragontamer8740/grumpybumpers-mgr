#! /usr/bin/env python3
import lxml
from bs4 import BeautifulSoup as bs

from sys import stdin

html=''

for line in stdin:
    html += line + '\n'

print(bs(html, 'lxml').prettify())
