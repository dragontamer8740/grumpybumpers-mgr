#! /usr/bin/env sh
AUTHCONF='./auth.cfg'
SETTINGSCONF='./settings.cfg'

PRETTYPROG='python3 ./pretty.py'
# settings.cfg can override things (like PRETTYPROG or AUTHCONF):
if [ -e "SETTINGSCONF" ]; then
  . "$SETTINGSCONF"
fi

if [ -e "$AUTHCONF" ]; then
  . "$AUTHCONF"
fi

if [ "$username" ] && [ "$password" ]; then
  DEBUG=''
  curl $DEBUG \
       --silent \
       -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0 SeaMonkey/2.53.7' \
       -H 'Referer: https://sig.grumpybumpers.com/' \
       -F 'username='"$username" \
       -F 'password='"$password" \
       -F '.submit=' \
       'https://sig.grumpybumpers.com/' | \
    python3 ./pretty.py | \
    grep '<img src=' | sed 's!. *<img src="!!;s!"/>.*!!'
else
  1>&2 echo "Error: User name and password not provided in auth.cfg, or auth.cfg not found."
fi